###
import numpy as np
import pyseis
###

## Starting Study Case
paramFilePath = './data/parameters.txt' #model parameters
case = pyseis.case(paramFilePath, arq='cpuv2')

## printing basic parameters
print (case.session)

## Loading Base Velocity Model From File
case.setRealVelFromFile('./data/vel_circulo.bin')


## reading  arbitrary  velocity vector form file
v0 = case.readVelFromFile('./data/vp_homogeneous_expand.bin')


# calling l2bb
fval, grad = case.l2bb(v0, cheap=True)

print('function value: {}'.format(fval))

#writing gradient vector as a binary file
case.writeVector(grad, './grad0.bin')

# saving gradient vector  as an image
case.showModel(grad, save='./gradModel0.png')

###
###
###
###
