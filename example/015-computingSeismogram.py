###
import numpy as np
import pyseis
###

## Starting Study Case
paramFilePath = './data/parameters.txt' #model parameters
case = pyseis.case(paramFilePath, arq='cpuv2')

## printing basic parameters
print (case.session)

## Loading Base Velocity Model From File
case.setRealVelFromFile('./data/vel_circulo.bin')

## border of seismic sources
print('seismic source border: {}'.format(case.session.sourceBorder))


cheap=True
## generating seismogram for the real model
sourcePosition = 110
seismogramVector= case.makeSeismogram(sourcePosition, cheap=cheap)

## showing seismogram
case.showSeismogram( seismogramVector, save='realSeismogram.png')
case.writeVector( seismogramVector, 'realSeismogram.bin')


## reading vector (velocity model) 
v0 = case.readVelFromFile('./data/vp_homogeneous_expand.bin')

## computing seismogram from model v0

seismogramVector0= case.makeSeismogram(sourcePosition, velVector=v0, cheap=cheap)
case.showSeismogram( seismogramVector0, save='seismogramV0.png')
case.writeVector( seismogramVector0, 'seismogramV0.bin')
