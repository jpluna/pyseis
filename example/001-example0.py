###
import numpy as np
import pyseis
###

## Starting Study Case
paramFilePath = './data/parameters.txt' #model parameters
case = pyseis.case(paramFilePath, arq='cpuv2')

## printing basic parameters
print (case.session)

## Loading Base Velocity Model From File
case.setRealVelFromFile('./data/vel_circulo.bin')
## Accessing Base Velocity Model Vector
rVector = case.state.realVel

## Saving Base Velocity Model Vector as figure
case.showModel(rVector, save='./realVel.png')


## reading  arbitrary  velocity vector form file
vector = case.readVelFromFile('./data/vp_homogeneous_expand.bin')
print(vector[:5])
print(vector.shape)

## Saving vector as figure
case.showModel(vector, save='./vel0.png')
