###
import numpy as np
import pyseis
###

## Starting Study Case
paramFilePath = './data/parameters.txt' #model parameters
case = pyseis.case(paramFilePath, arq='cpuv2')

## printing basic parameters
print (case.session)

## Loading Base Velocity Model From File
case.setRealVelFromFile('./data/vel_circulo.bin')

## reading  arbitrary  velocity vector form file
v0 = case.readVelFromFile('./data/vp_homogeneous_expand.bin')

fval, grad = case.l2bb(v0, cheap=True)
# calling l2bb

step = 50
nIter = 2
for i in range(nIter):
    v0 = v0 - step * grad
    step /=2
    fval, grad = case.l2bb(v0, cheap=True)

# saving grad image
case.showModel(v0, save='./vFinal.png')
case.writeVector(v0, './vFinal.bin')
case.showModel(grad, save='./gradFinal.png')
case.writeVector(grad, './gradFinal.bin')
print('final value={}\n gradInfNorm={}'.format(fval, np.abs(grad).max()))
###
