#ifndef FWI_CORE_H
#define FWI_CORE_H


typedef void (cerjanCore)(float *p2, float *p3, int NZ, int NX, int HALF_STENCIL, int CERJAN_POINTS, float CERJAN_FACTOR);

typedef void (modelSeismogramCore)(float *h_p1, float *h_p2, float *h_p3, float *h_vel, float *h_ricker, float *h_seis,
                                   int NZ, int NX, int NT, int SOURCE_V, int SOURCE_H, int RECEIVERS, int HALF_STENCIL, int CERJAN_POINTS, int DECIMA,
                                   float DT, float DX, float PEAK_FREQ, float CERJAN_FACTOR, float RICKER_DELAY, float C0, float C1, float C2, float C3, float C4);

typedef void (adjSourceCore)(float *h_seis_A, float *h_seis_B, float *h_adj, int NX, int NT, int HALF_STENCIL, int DECIMA);

typedef void (modelDerivCore)(float *h_p1, float *h_p2, float *h_p3, float *h_calc_vel, float *h_ricker, float *h_derivative, float *h_snapshots,
                              int NZ, int NX, int NT, int SOURCE_V, int SOURCE_H, int RECEIVERS, int HALF_STENCIL, int CERJAN_POINTS, int DECIMA,
                              float DT, float DX, float PEAK_FREQ, float CERJAN_FACTOR, float RICKER_DELAY, float C0, float C1, float C2, float C3, float C4);

typedef void (correlationCore)(float *h_p1, float *h_p2, float *h_p3, float *h_calc_vel, float *h_ricker, float *h_snapshots, float *h_correlation, float *h_adj,
                               int NZ, int NX, int NT, int SOURCE_V, int SOURCE_H, int RECEIVERS, int HALF_STENCIL, int CERJAN_POINTS, int DECIMA,
                               float DT, float DX, float PEAK_FREQ, float CERJAN_FACTOR, float RICKER_DELAY, float C0, float C1, float C2, float C3, float C4);

typedef void (normL2Core)(float *h_L2, float *h_adj, int NX, int NT, int HALF_STENCIL, int DECIMA);

typedef float (misfitCore)(float *h_seis_A, float *h_seis_B, int NX, int NT, int HALF_STENCIL, int DECIMA);

typedef void (gradientCore)(float *h_calc_vel, float *h_correlation, float *h_grad, int NZ, int NX, int HALF_STENCIL, int HEIGTH_Z);


typedef struct
{
    cerjanCore *cerjan;
    adjSourceCore *adjSource;
    adjSourceCore *adjSource2;
    modelDerivCore *modelDeriv;
    modelDerivCore *modelDerivGlobal;
    correlationCore *correlation;
    modelSeismogramCore *modelSeismogram;
    modelSeismogramCore *modelSeismogramGlobal;
    normL2Core *normL2;
	misfitCore *misfit;
    gradientCore *gradient;
}fwiCore;

#endif
