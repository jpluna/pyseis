#ifndef DATA_PRINT_H
#define DATA_PRINT_H


void writeImage(float *img, int NZ, int NX, char * fileName);


void printWavelet(float *wavelet, int NT, float DT, float PEAK_FREQ, float RICKER_DELAY);


void printSeismogram(float *seis, int NX, int NT, int SOURCE_H, int DECIMA);


void printSmoothSeismogram(float *seis, int NX, int NT, int SOURCE_H, int DECIMA);


void printAdjointSource(float *seis, int NX, int NT, int SOURCE_H, int DECIMA);


void printCorrelation(float *seis, int NZ, int NX);


void printGradient(float *seis, int NZ, int NX);


void printCalcVel(float *seis, int NZ, int NX);


#endif
