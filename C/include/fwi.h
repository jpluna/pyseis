#ifndef FWI_H
#define FWI_H

#include <fwi_core.h>


float FWI(float *h_p1, float *h_p2, float *h_p3, float *h_vel, float *h_calc_vel, float *h_ricker, float *h_seis_A, float *h_seis_B, float *h_derivative, float *h_snapshots, float *h_correlation, float *h_grad, float *h_L2, float *h_adj,
          int NZ, int NX, int NT, int SOURCE_V, int SOURCE_H, int RECEIVERS, int HALF_STENCIL, int CERJAN_POINTS, int BORDER, int DECIMA, int HEIGTH_Z,
          float DT, float DX, float PEAK_FREQ, float CERJAN_FACTOR, float RICKER_DELAY, float C0, float C1, float C2, float C3, float C4, fwiCore *core, int cheap);


#endif          
