#ifndef DATA_READ_H
#define DATA_READ_H


void velReading(char *velFilePath, float *vel, int NZ, int NX);


void smoothVelReading(char *vel0FilePath, float *vel, int NZ, int NX);


#endif
