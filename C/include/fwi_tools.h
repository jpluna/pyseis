#ifndef FWI_TOOLS_H
#define FWI_TOOLS_H

#include<fwi_core.h>




typedef struct
{
    char *tmpDir;


    int NZ;// discritization of the depth.
    int NX;// discretization of the surface.
    int NT;// size of the time simple (discritization of the recording time)
    int SOURCE_V;//coordinates of the sourace
    int SOURCE_H; 
    int RECEIVERS;// depth of the receivers
    int HALF_STENCIL;
    int CERJAN_POINTS;// size of cerjan border
    int BORDER;// space between sourcer and cerjan border
    int DECIMA;
    int HEIGTH_Z;
    int ret;

    float DT;
    float DX;
    float PEAK_FREQ;
    float CERJAN_FACTOR;
    float RICKER_DELAY;

    float C0;
    float C1;
    float C2;
    float C3;
    float C4;


}fwiSession;



typedef struct
{
	// basic state variables
    float *h_p1;
    float *h_p2;
    float *h_p3;
    float *h_vel;// velocity model;
    float *h_calc_vel;//velocity model approximation along iterations;
    float *h_ricker;
    float *h_seis_A;// real seismogram (from real model)
    float *h_seis_B;// seismogram computed from h_calc_vel
    float *h_derivative;
    float *h_snapshots;
    float *h_correlation;
    float *h_grad;
    float *h_L2;
    float *h_adj;
    float *h_wavelet;

	//for GPU
    float *d_p1;
    float *d_p2;
    float *d_p3;
    float *d_vel;// velocity model;
    float *d_calc_vel;//velocity model approximation along iterations;
    float *d_ricker;
    float *d_seis_A;// real seismogram (from real model)
    float *d_seis_B;// seismogram computed from h_calc_vel
    float *d_derivative;
    float *d_snapshots;
    float *d_correlation;
    float *d_grad;
    float *d_L2;
    float *d_adj;

	// for MPI
	int rank;
	int size;

}fwiState;




fwiSession * initSession(char * paramFilePath);
//fwiState * initState(fwiSession * session); // these functions are provided by each core code


// data management

void printWaveletSS(fwiSession *session, fwiState *state);
void velReadingSS(char * velFilePath, float *vel_vector, fwiSession *session);
float FWISS(fwiSession *session, fwiState *state, fwiCore *myCore, int cheap);

float FWISS2(fwiSession *session, fwiState *state, fwiCore *myCore, int cheap, void *context);

void calcSeismogramSS(float *velVector, float * seisVector, int SOURCE_H, fwiSession *session, fwiState *state, fwiCore *myCore, int  cheap);

void writeVectorSS(float *vector, int n, char *fileName);
#endif
          

