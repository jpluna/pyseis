#ifndef NVDGPU_CORE_H 
#define NVDGPU_CORE_H


#include<fwi_tools.h>
fwiCore * initNVDGPUCore();

fwiState * initNVDCPUState(fwiSession * session);

void NVDGPU_cerjan(float *p2, float *p3, int NZ, int NX, int HALF_STENCIL, int CERJAN_POINTS, float CERJAN_FACTOR);

void NVDGPU_modelSeismogram(float *h_p1, float *h_p2, float *h_p3, float *h_vel, float *h_ricker, float *h_seis,
                         int NZ, int NX, int NT, int SOURCE_V, int SOURCE_H, int RECEIVERS, int HALF_STENCIL, int CERJAN_POINTS, int DECIMA,
                         float DT, float DX, float PEAK_FREQ, float CERJAN_FACTOR, float RICKER_DELAY, float C0, float C1, float C2, float C3, float C4);

void NVDGPU_adjSource(float *h_seis_A, float *h_seis_B, float *h_adj, int NX, int NT, int HALF_STENCIL, int DECIMA);

void NVDGPU_modelDeriv(float *h_p1, float *h_p2, float *h_p3, float *h_calc_vel, float *h_ricker, float *h_derivative, float *h_snapshots,
                    int NZ, int NX, int NT, int SOURCE_V, int SOURCE_H, int RECEIVERS, int HALF_STENCIL, int CERJAN_POINTS, int DECIMA,
                    float DT, float DX, float PEAK_FREQ, float CERJAN_FACTOR, float RICKER_DELAY, float C0, float C1, float C2, float C3, float C4);

void NVDGPU_correlation(float *h_p1, float *h_p2, float *h_p3, float *h_calc_vel, float *h_ricker, float *h_snapshots, float *h_correlation, float *h_adj,
                     int NZ, int NX, int NT, int SOURCE_V, int SOURCE_H, int RECEIVERS, int HALF_STENCIL, int CERJAN_POINTS, int DECIMA,
                     float DT, float DX, float PEAK_FREQ, float CERJAN_FACTOR, float RICKER_DELAY, float C0, float C1, float C2, float C3, float C4);

void NVDGPU_normL2(float *h_L2, float *h_adj, int NX, int NT, int HALF_STENCIL, int DECIMA);

void NVDGPU_gradient(float *h_calc_vel, float *h_correlation, float *h_grad, int NZ, int NX, int HALF_STENCIL, int HEIGTH_Z);


#endif
