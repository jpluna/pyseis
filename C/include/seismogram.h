#ifndef SEISMOGRAM_H
#define SEISMOGRAM_H

#include <fwi_core.h>


void obsSeismogram(float *h_p1, float *h_p2, float *h_p3, float *h_vel, float *h_ricker, float *h_seis_A,
                   int NZ, int NX, int NT, int SOURCE_V, int SOURCE_H, int RECEIVERS, int HALF_STENCIL, int CERJAN_POINTS, int DECIMA,
                   float DT, float DX, float PEAK_FREQ, float CERJAN_FACTOR, float RICKER_DELAY, float C0, float C1, float C2, float C3, float C4, fwiCore *core, int cheap);
// compute the sesismogram for the original volocity model

void calcSeismogram(float *h_p1, float *h_p2, float *h_p3, float *h_calc_vel, float *h_ricker, float *h_seis_B,
                    int NZ, int NX, int NT, int SOURCE_V, int SOURCE_H, int RECEIVERS, int HALF_STENCIL, int CERJAN_POINTS, int DECIMA,
                    float DT, float DX, float PEAK_FREQ, float CERJAN_FACTOR, float RICKER_DELAY, float C0, float C1, float C2, float C3, float C4, fwiCore *core, int cheap);
// compute the seismogram fo the current velocity model (in h_calc_vel) and stores the seismogram in 


#endif
