#ifndef OBJECTIVEFUNC_H
#define OBJECTIVEFUNC_H

#include <fwi_core.h>


float calcNormL2(float *h_L2, float *h_adj, int NX, int NT, int HALF_STENCIL, int DECIMA, fwiCore *core);


#endif
