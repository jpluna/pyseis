from libc.stdlib cimport malloc, free

## global elements
cdef object py_misfit= None #This for the misfit function
cdef object py_adjSourceBuilder= None #This for the adjoint source 

cdef extern from "fwi_core.h": 
    ctypedef float (*misfitCoreP)(float *h_seis_A, float *h_seis_B, int NX, int NT, int HALF_STENCIL, int DECIMA) 
    ctypedef void (*adjSourceCoreP)(float *h_seis_A, float *h_seis_B, float *h_adj, int NX, int NT, int HALF_STENCIL, int DECIMA)

    ctypedef struct fwiCore:
        adjSourceCoreP adjSource2 
        misfitCoreP misfit
        pass

cdef extern from "fwi_tools.h": 
    ctypedef struct fwiSession: 
        int NX
        int NZ
        int NT
        int CERJAN_POINTS
        int BORDER
        int DECIMA
        pass 

    ctypedef struct fwiState: 
        float *h_vel
        float * h_calc_vel
        float * h_grad
        float *h_seis_A
        float *h_seis_B
        float *h_adj
        pass 

    fwiSession * initSession(char * paramFilePath) 
    void velReadingSS(char * velFilePath, float *vel_vector, fwiSession *session) 
    float FWISS(fwiSession *session, fwiState *state, fwiCore *myCore, int cheap)
    float FWISS2(fwiSession *session, fwiState *state, fwiCore *myCore, int cheap, void *context)
    void calcSeismogramSS(float *velVector, float * seisVector, int SOURCE_H, fwiSession *session, fwiState *state, fwiCore *myCore, int  cheap)
    void writeVectorSS(float *vector, int n, char *fileName)

cdef extern from "CPU_core.h": 
    fwiCore * initCPUCore()
    fwiState * initCPUState(fwiSession * session);

cdef extern from "CPU_core2.h": 
    fwiCore * initCPUc2Core()
    fwiState * initCPUc2State(fwiSession * session);

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os


cdef float py_misfitCore(float *h_seis_A, float *h_seis_B, int NX, int NT, int HALF_STENCIL, int DECIMA):
    cdef float res =0
    cdef int n
    cdef int i
    n = (NT * NX) // DECIMA

    modelSeis = np.empty(n, dtype=np.float32) 
    realSeis = np.empty_like(modelSeis) 

    for i in range(n):
        modelSeis[i] = h_seis_A[i]
        realSeis[i] = h_seis_B[i]

    if py_misfit is not None:
        res = py_misfit(modelSeis, realSeis)

    return res 

cdef void py_adjSourceBuilderCore(float *h_seis_A, float *h_seis_B, float *h_adj, int NX, int NT, int HALF_STENCIL, int DECIMA):

    cdef int n
    cdef int i
    n = (NT * NX) // DECIMA

    modelSeis = np.empty(n, dtype=np.float32) 
    realSeis = np.empty_like(modelSeis) 

    for i in range(n):
        modelSeis[i] = h_seis_A[i]
    if py_adjSourceBuilder is not None: 
        adjSource =  py_adjSourceBuilder(modelSeis, realSeis)

        for i in range(n):
            h_adj[i] = adjSource[i]

    return 

class pySession():
    param=None
    sourceBorder=None
    def __init__(self):
        pass

    def __str__(self):
        ss = 'Parameters:\n\n{}\n SourceBorder:{}\n'.format(self.param, self.sourceBorder)
        return ss

    def __repr__(self):
        return self.__str__()

class pyState():
    realVel=None # np.array for saving real velocity
    realSeismogram=None # np.array for the real seismogram h_seis_B
    modelSeismogram=None # np.array for the real seismogram h_seis_A
    adjSorce=None # 
    def __init__(self):
        pass


cdef class case(): 
    cdef fwiSession * cSession
    cdef fwiState * cState
    cdef fwiCore * cCore
    cdef public:
        object state, session


    def __init__ (self, paramFilePath, arq='cpu'):
        if not arq in ['cpu', 'cpuv2']:
            print('Option {} not available'.format(arq))
        else:
            if not os.path.isfile(paramFilePath):
                print('Error: parameters file {} not found'.format(paramFilePath))
            else:
                self.__initSession__(paramFilePath)
                self.__initState__(arq=arq)
                self.__setCore__(arq=arq)

    def __initSession__(self, paramFilePath):
        cdef bytes py_filePath
        cdef char *cFilePath 
        py_filePath = paramFilePath.encode()
        cFilePath = py_filePath
        self.cSession = initSession(cFilePath)

        #defining pySession

        self.session = pySession()
        aux = {'nx': self.cSession.NX, 
                'nz': self.cSession.NZ, 
                'nt': self.cSession.NT, 
                'cerjanBorder': self.cSession.CERJAN_POINTS, 
                'border': self.cSession.BORDER, 
                'decima': self.cSession.DECIMA} 

        self.session.param = pd.Series(aux)
        param = self.session.param
        totalBorder = param.loc[['cerjanBorder','border']].sum()
        self.session.sourceBorder= [totalBorder, param.loc['nx'] -totalBorder -1]

    def __initState__(self, arq='cpu'):
        if arq == 'cpu':
            self.cState = initCPUState( self.cSession) 
        elif arq == 'cpuv2':
            self.cState = initCPUc2State( self.cSession) 

        # definig pystate
        self.state = pyState()

        #changing h_vel into pyState
        cdef int NX = self.cSession.NX
        cdef int NZ = self.cSession.NZ
        vector = np.empty(NX * NZ, dtype=np.float32)
        self.state.realVel = vector
        cdef float [:] vV = vector 

        free(self.cState.h_vel)
        self.cState.h_vel = &vV[0]

        # freeing h_calc_vel
        print('warning: c h_calc_vel  was set free')
        free(self.cState.h_calc_vel)
        print('warning: h_grad  was set free')
        free(self.cState.h_grad)


    def __setCore__(self, arq='cpu'):
        if arq == 'cpu':
            self.cCore = initCPUCore()
        elif arq == 'cpuv2':
            self.cCore = initCPUc2Core()

        print('{} core set successfully'.format(arq))
        

    def __readVelFromFile__(self, velFilePath, vector=None): 

        cdef bytes py_filePath
        cdef char *cFilePath 
        py_filePath = velFilePath.encode()
        cFilePath = py_filePath

        if vector is None: 
            vector = np.empty(self.cSession.NX * self.cSession.NZ, dtype=np.float32)

        cdef float [:] vV = vector 

        velReadingSS(cFilePath, &vV[0],  self.cSession)

        return vector


    def readVelFromFile(self, velFilePath, vector=None): 
        if not os.path.isfile(velFilePath):
            print('Error: velocity file {} not found'.format(velFilePath))
        else:
            vector = self.__readVelFromFile__(velFilePath, vector=vector) 
        return vector

    def setRealVelFromFile(self, velFilePath):
        return self.readVelFromFile(velFilePath, vector=self.state.realVel)

    def vel2matrix(self, vector):
        nx = self.cSession.NX 
        nz = self.cSession.NZ
        return vector.reshape(nz, nx)


    def showModel(self, vector, ax=None, save=None):
        import matplotlib.pyplot as plt
        if ax is None:
            fig = plt.figure()
            ax = fig.gca()
        # ax.cla()

        matrix = self.vel2matrix(vector)

        # plt.imshow(matrix, ax=ax, interpolation='nearest')
        aux = ax.imshow(matrix, interpolation='nearest')
        plt.colorbar(aux, ax=ax)
        if save is not None:
            plt.savefig(save)

    def bbFromAdjSource(self, vector, adjSourceBuilder=None, misfit=None, cheap=True):

        grad = np.empty_like(vector, dtype=np.float32)
        cdef void *context

        cdef float [:] vV = vector 
        cdef float [:] vG = grad 
        cdef float fVal
        cdef int cCheap

        global py_misfit
        global py_adjSourceBuilder

        if misfit is not None: 
            py_misfit = misfit 
        else:
            py_misfit = l2Misfit

        if adjSourceBuilder is not None: 
            py_adjSourceBuilder = adjSourceBuilder 
        else:
            py_adjSourceBuilder = l2AdjSourceBuilder

        if cheap:
            cCheap = 1
        else:
            cCheap = 0

        self.cState.h_calc_vel = &vV[0]
        self.cState.h_grad = &vG[0]
        # self.cCore.adjSource2 = <adjSourceCoreP> &py_adjSourceBuilderCore
        # self.cCore.misfit = <misfitCoreP> &py_misfitCore

        self.cCore.adjSource2 =  &py_adjSourceBuilderCore
        self.cCore.misfit =  &py_misfitCore
        fVal = FWISS2( self.cSession,  self.cState, self.cCore, cCheap, context)

        return fVal, grad

    def l2bb(self, vector, cheap=True):
        # allocating memory for gradient
        grad = np.empty_like(vector, dtype=np.float32)

        cdef float [:] vV = vector 
        cdef float [:] vG = grad 
        cdef float fVal
        cdef int cCheap

        if cheap:
            cCheap = 1
        else:
            cCheap = 0


        self.cState.h_calc_vel = &vV[0]
        self.cState.h_grad = &vG[0]
        fVal = FWISS( self.cSession,  self.cState, self.cCore, cCheap)

        return fVal, grad

    def makeSeismogram(self, sourcePosition, velVector=None, seisVector=None, cheap=True):
        '''
        if velVector is None, real velocity model will be considered
        seis matrix: (horizonta seismograms) (nx, nt/decima)
        dimension of 
        '''
        cdef int cCheap

        if cheap:
            cCheap = 1
        else:
            cCheap = 0


        if velVector is None:
            velVector = self.state.realVel
        if seisVector is None:
            nt = self.session.param.loc['nt']
            nx = self.session.param.loc['nx']
            decima = self.session.param.loc['decima']
            seisVector = np.empty((nt * nx) // decima, dtype=np.float32)

        cdef float [:] velVectorV = velVector
        cdef float [:] seisVectorV = seisVector 
        calcSeismogramSS(&velVectorV[0], &seisVectorV[0], sourcePosition,  self.cSession,  self.cState, self.cCore, cCheap)
        return seisVector

    def seis2matrix(self, seisVector):
        nx = self.cSession.NX 
        return seisVector.reshape(-1,nx)

    def showSeismogram(self, seisVector, ax=None, save=None):
        import matplotlib.pyplot as plt

        if ax is None:
            fig = plt.figure()
            ax = fig.gca()
        # ax.cla()

        matrix = self.seis2matrix(seisVector)

        # plt.imshow(matrix, ax=ax, aspect='auto',cmap ='gray', interpolation='nearest', vmin=-2, vmax=2)
        aux = ax.imshow(matrix,  aspect='auto',cmap ='gray', interpolation='nearest', vmin=-2, vmax=2)
        plt.colorbar(aux, ax=ax)
        if save is not None:
            plt.savefig(save)

    def writeVector(self, vector, fileName):
        cdef bytes py_filePath
        cdef char *cFilePath 
        py_filePath = fileName.encode()
        cFilePath = py_filePath

        n = len(vector)
        cdef float [:] vV = vector 
        writeVectorSS( &vV[0], n, cFilePath)



    
def l2AdjSourceBuilder(modelSeis, realSeis):
    adjSource = modelSeis - realSeis
    return adjSource

def l2Misfit(modelSeis, realSeis):
    aux = (modelSeis - realSeis).reshape(-1,)
    return 0.5 * np.dot(aux, aux)

