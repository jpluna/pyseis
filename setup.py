import setuptools
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy

import platform
import os

osy =  platform.system() 

suppOS = ['Darwin', 'Linux']
if osy not in suppOS: 
    raise RuntimeError("Unsupported operating system ")

if osy == 'Darwin': 
    extra_objects=['./C/lib/osx/cpufwilib.a']
else:
    extra_objects=['./C/lib/linux/cpufwilib.a']



sourcefiles = [
        './pyseis/pyseis.pyx',
        # './C/source/seismicModel.c'
        # './C/source/vectorOperations.c',
        ]

extensions = [
        Extension('pyseis', 
            sourcefiles,
            # include_dirs=['./C/include/', numpy.get_include(), '/usr/include/mpi'],
            include_dirs=['./C/include/', numpy.get_include(),],
            # language='c++',
            # libraries=['mpi']
            # libraries=['gfortran', 'm'],
            # library_dirs=['./algencanLib/osx/'] 
            extra_objects=extra_objects,
            # extra_objects=['./C/lib/cpufwilib.a']
            # extra_link_args=["-lm -lgfortran"],
            # extra_link_args=["-lm "],
            ),
        ]
setup( 
        name='pyseis', 
        version='0.0.9.dev1', 
        description='seismic', 
        author='Juan Pablo Luna and Lucas Miranda', 
        author_email='jpluna@yandex.com', 
        license='MIT', 
        packages=['pyseis',], 
        ext_modules=cythonize(extensions),
        install_requires=['cython', 'numpy', 'matplotlib']
        )
